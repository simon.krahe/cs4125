from startup import Startup

Startup.startupDeclarations()

inhabitant1 = Startup.userhandler.addInhabitant("Joe")
admin1 = Startup.userhandler.addAdmin("Kevin")

# Proof of concept for availability
# This is purposefully commented out code. Uncomment it to test the proof of concept availability implementation.
"""
admin1.addAppliance("OsramLightbulb", "Dead Lightbulb")
Startup.scenehandler.getSceneList()[1].debugForceRemoveApplianceInFirstCommandObject() #force removing the reference to the appliance in the command object of the turn on scene
inhabitant1.triggerScene("Dead Lightbulb: Turn On")
"""

# Admin :
admin1.addAppliance("OsramLightbulb", "Living Room Light")
admin1.addAppliance("OsramLightbulb", "Living Room Light") # Duplicate, is discarded
admin1.addAppliance("OsramLightbulb", "Bathroom Light")
admin1.addAppliance("Thermostat", "Kitchen Thermostat")
admin1.addAppliance("Toaster", "Toaster")
admin1.addAppliance("BinaryLightbulb", "Kitchen Light")
admin1.triggerScene("Impossible Scene") # Scene not found
admin1.removeAppliance("Living Room Light")
admin1.removeAppliance("Impossible Appliance")  # Appliance not found and not removed
externalServiceAmazon = admin1.addExternalService("Amazon", "Amazon")

# Inhabitant :
inhabitant1.triggerScene("Living Room Light: Turn On")                              # not found, where is the list ?
inhabitant1.triggerScene("Non existing scene") # Scene that can not be found
inhabitant1.triggerScene("Kitchen Thermostat: Get current temperature reading")     # not found, where is the list ? 
inhabitant1.triggerScene("Kitchen Thermostat: Get target temperature reading")

# Scheduler :
admin1.addScheduledEvent("Living Room Light: Turn On", Startup.scheduler.currentMinute()+2)
Startup.scheduler.addEvent("Living Room Light: Turn On",Startup.scheduler.currentMinute()+1)
Startup.scheduler.addEvent("Living Room Light: Turn Off", Startup.scheduler.currentMinute()+1)

# External Services :
admin1.addExternalService("Amazon", "Amazon") # Already exist
externalServiceGoogleHome = admin1.addExternalService("GoogleHome", "GoogleHome")

# Amazon :
externalServiceAmazon.receiveOrder("Buy chocolate", "Iridium", "TeamIridium")
externalServiceAmazon.login("Iridium", "Team Iridium")
externalServiceAmazon.triggerScene("Living Room Light: Turn On")

# Google Home :
externalServiceGoogleHome.login("Iridium", "Team Iridium")
externalServiceGoogleHome.triggerScene("Living Room Light: Turn On")
externalServiceGoogleHome.receiveOrder("Listen to music")
externalServiceGoogleHome.receiveOrder("No order") # no order done
externalServiceGoogleHome.configurationGoogleHome("Configuration of the voice")
externalServiceGoogleHome.configurationGoogleHome("no order")  # order not done


print()
Startup.startUIAndScheduler()