from startup import Startup
import unittest
import os
from contextlib import redirect_stdout

Startup.startupDeclarations()
appliancehandler = Startup.appliancehandler
scenehandler = Startup.scenehandler
admin1 = Startup.userhandler.addAdmin("Kevin")
testfile = "testfile.txt"

number_appliance = len(appliancehandler._ApplianceHandler__applianceList)
admin1.addAppliance("OsramLightbulb", "Testing Lightbulb")
number_scene = len(scenehandler._SceneHandler__sceneList)

externalServiceAmazon = admin1.addExternalService("Amazon", "Amazon")
externalServiceGoogleHome = admin1.addExternalService("GoogleHome", "GoogleHome")

def testExpectedOutput(text):
    file = open(testfile)
    contents = file.read()
    file.close()
    os.remove(testfile)
    if (contents == text):
        return True
    return False

class Test(unittest.TestCase):

    def testAddAppliance(self):
        self.assertTrue(number_appliance + 1 == len(appliancehandler._ApplianceHandler__applianceList))

    def testRemoveAppliance(self):
        admin1.removeAppliance("Testing Lightbulb")
        self.assertTrue(number_appliance == len(appliancehandler._ApplianceHandler__applianceList))

    def testTriggerScene(self):
        with open(testfile, 'w') as out:
            with redirect_stdout(out):
                admin1.triggerScene("TestingLightbulb: Turn On")
        test = testExpectedOutput("Scene TestingLightbulb: Turn On not found\n")
        self.assertTrue(test)
        
    def testRemoveScene(self):
        admin1.removeScene("Testing Lightbulb: Turn Off")
        self.assertTrue(number_scene - 1 == len(scenehandler._SceneHandler__sceneList))

    def testReceiveOrder1(self):
        with open(testfile, 'w') as out:
            with redirect_stdout(out):
                externalServiceAmazon.receiveOrder("Buy chocolate", "Iridium", "TeamIridium")
        testIf = testExpectedOutput("Iridium is connected\nOrder: Buy chocolate is done\n")
        self.assertTrue(testIf)

    def testReceiveOrder2(self):
        with open(testfile, 'w') as out:
            with redirect_stdout(out):
                externalServiceAmazon.receiveOrder("Buy chocolate", "Iridium", "")
        testElse = testExpectedOutput("your password is incorrect\n")
        self.assertTrue(testElse)

    def testLogin1(self):
        with open(testfile, 'w') as out:
            with redirect_stdout(out):
                externalServiceAmazon.login("Iridium", "Team Iridium")
        testIfIf = testExpectedOutput("Iridium is connected\n")
        self.assertTrue(testIfIf)

    def testLogin2(self):
        with open(testfile, 'w') as out:
            with redirect_stdout(out):
                externalServiceAmazon.login("Iridium", "")
        testIfElse = testExpectedOutput("your password is incorrect\n")
        self.assertTrue(testIfElse)

    def testLogin3(self):
        with open(testfile, 'w') as out:
            with redirect_stdout(out):
                externalServiceAmazon.login("", "Team Iridium")        
        testElse = testExpectedOutput("your username is incorrect\n")
        self.assertTrue(testElse)

    def testConfigurationGoogleHome1(self):
        with open(testfile, 'w') as out:
            with redirect_stdout(out):
                externalServiceGoogleHome.configurationGoogleHome("Configuration of the voice")
        testIf = testExpectedOutput("Google Home : configuration of the voice\n")
        self.assertTrue(testIf)

    def testConfigurationGoogleHome2(self):
        with open(testfile, 'w') as out:
            with redirect_stdout(out):
                externalServiceGoogleHome.configurationGoogleHome("No configuration")
        testElse = testExpectedOutput("Google Home can not change this configuration\n")
        self.assertTrue(testElse)

if __name__ == '__main__':
    unittest.main()
