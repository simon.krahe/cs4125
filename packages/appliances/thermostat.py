class Thermostat:
    def __init__(self, identifier):
        self.__identifier = identifier
        self.__targetedTemp = 20
        self.__currentTemp = 20

    def getIdentifier(self):
        return self.__identifier

    def setIdentifier(self, newIdentifier):
        self.__identifier = newIdentifier

    def getTargetedTemp(self):
        return self.__targetedTemp

    def getCurrentTemp(self):
        return self.__currentTemp

    def setTargetedTemp(self, temp):
        self.__targetedTemp = temp

    def setCurrentTemp(self, temp):
        self.__currentTemp = temp