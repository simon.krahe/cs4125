class BinaryLightbulb:
    def __init__(self):
        self.__turnedOn = False
        self.__licenseKey="cs4125"

    def checkLicenseKey(self, key):
        if key == self.__licenseKey:
            return True
        return False

    def getTurnedOn(self, key):
        if(self.checkLicenseKey(key)):
            return self.__turnedOn
    
    def toggle(self, key):
        if(self.checkLicenseKey(key)):
            self.__turnedOn = not(self.__turnedOn)
            print("Toggled")
    
    def on(self, key):
        if(self.checkLicenseKey(key)):
            self.__turnedOn = True
            print("Turned on")
    
    def off(self, key):
        if(self.checkLicenseKey(key)):
            self.__turnedOn = False
            print("Turned off")
