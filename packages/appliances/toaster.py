class Toaster:
    def __init__(self, identifier):
        self.__identifier = identifier
        self.__temperature = 40
        self.__timer = 2
    
    def getIdentifier(self):
        return self.__identifier
    
    def getTemperature(self):
        return self.__temperature

    def getTimer(self):
        return self.__timer
    
    def setTemperature(self, temperature):
        if temperature < 0:
            self.__temperature = 0
        elif temperature > 100:
            self.__temperature = 100
        else:
            self.__temperature = temperature
    
    def setTimer(self, time):
        if time < 0:
            self.__timer = 0
        elif time > 10:
            self.__timer = 10
        else:
            self.timer = time
    
    def toast(self):
        print("Now toasting for " + str(self.__timer) + " on temperature level " + str(self.__temperature))
    
    def cancelToasting(self):
        print("Cancelled current toast")
