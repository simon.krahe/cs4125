class OsramLightbulb:
    def __init__(self, identifier):
        self.__identifier = identifier
        self.__turnedOn = False
        self.__brightness = 100
        self.__colorRGB = (255,255,255)

    def getIdentifier(self):
        return self.__identifier

    def setIdentifier(self, newIdentifier):
        self.__identifier = newIdentifier

    def getTurnedOn(self):
        return self.__turnedOn

    def getBrightness(self):
        return self.__brightness

    def getColor(self):
        return self.__colorRGB

    def toggleOn(self):
        print("On state toggled")
        self.__turnedOn = not(self.__turnedOn)

    def switchOff(self):
        print("Switched off")
        self.__turnedOn = False

    def switchOn(self):
        print("Switched on")
        self.__turnedOn = True

    def setBrightness(self, brightness):
        if brightness < 0:
            self.__brightness = 0
        elif brightness > 100:
            self.__brightness = 100
        else:
            self.__brightness = brightness
        print("Set brightness")

    def setColor(self, RGB):
        for i in range(3):
            if RGB[i] < 0:
                self.__colorRGB[i] = 0
            elif RGB[i] > 255:
                self.__colorRGB[i] = 255
            else:
                self.__colorRGB[i] = RGB[int(i)]
        print("Set color")