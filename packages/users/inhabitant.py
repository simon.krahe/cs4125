from externalServices.externalServiceHandler import ExternalServiceHandler


class Inhabitant:
    def __init__(self, name, sceneHandler):
        self.__sceneHandler = sceneHandler
        self.__name = name

    def triggerScene(self, identifier):
        self.__sceneHandler.triggerScene(identifier)

    def getName(self):
        return self.__name

    def setName(self, newName):
        self.__name = newName
    
