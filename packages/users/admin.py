from users.inhabitant import Inhabitant

class Admin(Inhabitant):
    def __init__(self, name, applianceHandler, sceneHandler, scheduler, externalServiceHandler):
        self.__applianceHandler = applianceHandler
        self.__sceneHandler = sceneHandler
        self.__name = name
        self.__scheduler = scheduler
        self.__externalServiceHandler = externalServiceHandler

    def getName(self):
        return self.__name

    def setName(self, newName):
        self.__name = newName

    def addAppliance(self, typeOfAppliance, identifier):
        self.__applianceHandler.addAppliance(typeOfAppliance, identifier)

    def removeAppliance(self, identifier):
        self.__applianceHandler.removeAppliance(identifier)

    def addScene(self, applianceIdentifierList, identifier, taskList):
        self.__sceneHandler.addNormalScene(applianceIdentifierList, identifier, taskList)

    def removeScene(self, identifier):
        self.__sceneHandler.removeScene(identifier)

    def modifyScene(self, newApplianceIdentifierList = [], newIdentifier = '', newTaskList = [], newParameterList = []):
        self.__sceneHandler.modifyScene(newApplianceIdentifierList, newIdentifier, newTaskList, newParameterList)

    def triggerScene(self, identifier):
        self.__sceneHandler.triggerScene(identifier)

    def addScheduledEvent(self, sceneIdentifier, minute):
        self.__scheduler.addEvent(sceneIdentifier, minute)

    def addExternalService(self, typeOfExternalService, identifier):
        return self.__externalServiceHandler.addExternalService(typeOfExternalService, identifier)

    def addTrigger(self, scene, commandObject, value, applianceIdentifier):
        self.__sceneHandler.getScene(scene).addTrigger(applianceIdentifier, commandObject, value)