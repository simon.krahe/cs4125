from users.inhabitant import Inhabitant
from users.admin import Admin
from notBusinessLayer.handler import HandlerClass


class UserHandler(HandlerClass):
    def __init__(self, applianceHandler, sceneHandler, scheduler, externalServiceHandler):
        self.__inhabitantList=[]
        self.__adminList=[]
        self.__applianceHandler=applianceHandler
        self.__sceneHandler=sceneHandler
        self.__scheduler=scheduler
        self.__externalServiceHandler=externalServiceHandler

    def addInhabitant(self, name):
        inhabitant=Inhabitant(name, self.__sceneHandler)
        self.__inhabitantList.append(inhabitant)
        return inhabitant

    def addAdmin(self, name):
        admin=Admin(name, self.__applianceHandler, self.__sceneHandler, self.__scheduler, self.__externalServiceHandler)
        self.__adminList.append(admin)
        return admin