import time as t
from notBusinessLayer.customIO import CustomIO as IO

class Scheduler:
    def __init__(self,sceneHandler):
        self.__scenehandler = sceneHandler
        self.__listEvents = []
        self.__buffer = []
        self.__schedulerOn = False
    
    def getListEvents(self):
        return self.__listEvents
    
    def getBufferContent(self):
        return self.__buffer
    
    def getSchedulerOn(self):
        return self.__schedulerOn

    # element of self.__listEvents : [sceneIdentifier, [time0[,time1,time2,...]]
    def addEvent(self, sceneIdentifier, minute):
        found = next((scene for scene in self.__listEvents if (scene[0] == sceneIdentifier)), None)
        if found != None:
            if minute not in self.__listEvents[self.__listEvents.index(found)][1]:
                self.__listEvents[self.__listEvents.index(found)][1].append(minute)
        else:
            self.__listEvents.append([sceneIdentifier, [minute]])

    def convertIntoMinute(self, dayInTheWeek, hour, minute): # dayInTheWeek [0-6], hour [0-23], minute [0-59]
        return dayInTheWeek*1440 + 60 * hour + minute

    def currentMinute(self):
        return (t.time() // 60) % 10080 + 1440 * 3
  
    def deactivateScheduler(self):
        self.__schedulerOn = False
        IO.output("Scheduler Off")

    def updateBuffer(self):
        self.__buffer = []
        currentDay = self.currentMinute()//1440 
        for event in self.__listEvents:
            for minute in event[1]:
                if currentDay <= minute//1440 < currentDay + 1:
                    self.__buffer.append([event[0],minute])
        IO.output("Buffer has been updated")

    def activateScheduler(self):
        self.updateBuffer()
        self.__schedulerOn = True
        IO.output("Scheduler On")
        while self.__schedulerOn:
            if len(self.__buffer) == 0:
                IO.output("Empty buffer")
                self.deactivateScheduler()
            for event in self.__buffer:
                if event[1] == self.currentMinute():
                    self.__scenehandler.triggerScene(event[0])
                    self.__buffer.remove(event)