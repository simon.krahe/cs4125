from externalServices.externalServiceHandler import ExternalServiceHandler
from scenes.sceneHandler import SceneHandler
from users.userHandler import UserHandler
from applianceSystem.applianceHandler import ApplianceHandler
from ui.ui import UI
from scheduler.scheduler import Scheduler
from scenes.mediator import Mediator

class Startup:
    @classmethod
    def startupDeclarations(cls):
        cls.scenehandler=SceneHandler()
        cls.externalServiceHandler=ExternalServiceHandler(cls.scenehandler)
        cls.mediator = Mediator()
        cls.appliancehandler=ApplianceHandler(cls.scenehandler)
        cls.scheduler=Scheduler(cls.scenehandler)
        cls.userhandler=UserHandler(cls.appliancehandler, cls.scenehandler, cls.scheduler, cls.externalServiceHandler)
        cls.ui=UI(cls.appliancehandler, cls.scenehandler, cls.userhandler)

    @classmethod
    def startUIAndScheduler(cls):
            cls.ui.main()
            cls.scheduler.activateScheduler()
    
    @classmethod
    def returnSceneHandler(cls):
        return cls.scenehandler

    @classmethod
    def getMediator(cls):
        return cls.mediator