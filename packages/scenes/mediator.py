from scenes.exceptions import NotFound
from notBusinessLayer.customIO import CustomIO as IO
from scenes.scene import Scene
from scenes.observer import Subject

class Mediator(Subject):
    def __init__(self):
        self.__mappingObjectObserver = [] # Struct : [[object1, [observer1, observer2, ...]], ...]
    
    def attach(self, obj, obs):
        association = next((assoc for assoc in self.__mappingObjectObserver if (assoc[0] == obj)), None)
        if association != None:
            self.__mappingObjectObserver[self.__mappingObjectObserver.index(association)][1].append(obs)
        else:
            self.__mappingObjectObserver.append([obj, [obs]])
    
    def detach(self, obj, obs):
        try:
            association = next((assoc for assoc in self.__mappingObjectObserver if (assoc[0] == obj)), None)
            if association == None:
                raise NotFound
            else:
                try:
                    self.__mappingObjectObserver[self.__mappingObjectObserver.index(association)][1].remove(obs)
                except NotFound:
                    IO.output("{} not find".format(obs))
        except NotFound:
            IO.output("{} not find".format(obj))
    
    def removeObject(self, obj):
        try:
            association = next((assoc for assoc in self.__mappingObjectObserver if (assoc[0] == obj)), None)
            if association == None:
                raise NotFound
            else:
                self.__mappingObjectObserver.remove(association)
        except NotFound:
            IO.output("{} not find".format(obj))
    
    def notify(self, sender):
        if isinstance(sender, Scene):
            self.reactOnScene(sender)

    def reactOnScene(self, scene):
        listAppliances = scene.getApplianceIdentifierList()
        for association in self.__mappingObjectObserver:
            if association[0] in listAppliances:
                for scene_ in association[1]:
                    scene_.update()
                    #NOTE when creating connection between appliance and scenes, scenes must be passed as object and not scene identifier