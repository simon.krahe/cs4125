from notBusinessLayer.customIO import CustomIO as IO
from scenes.observer import Observer
class Scene(Observer):
    def __init__(self, applianceIdentifierList, identifier, taskList, type, invoker, mediator):
        self.__applianceIdentifierList = applianceIdentifierList
        self.__identifier = identifier
        self.__taskList = taskList
        self.__type = type
        self.__invoker = invoker
        self.__triggerList = [] # struct : [[applianceIdentifier, [[commandObjectA, stateX], [commandObjectB, stateY],...], ...]
        self.__mediator = mediator

    def getApplianceIdentifierList(self):
        return self.__applianceIdentifierList

    def setApplianceIdentifierList(self, newApplianceIdentifierList):
        self.__applianceIdentifierList = newApplianceIdentifierList

    def getIdentifier(self):
        return self.__identifier

    def setIdentifier(self, newIdentifier):
        self.__identifier = newIdentifier

    def getTaskList(self):
        return self.__taskList

    def setTaskList(self, newTaskList):
        self.__taskList = newTaskList

    def getType(self):
        return self.__type

    def addTrigger(self, applianceIdentifier, commandObject, state):
        self.__triggerList.append([applianceIdentifier, commandObject, state])
        self.__mediator.attach(applianceIdentifier, self)

    def execute(self):
        IO.output("Executing scene " + self.getIdentifier())
        self.__invoker.trigger(self.__taskList)
        self.__mediator.notify(self)
    
    def update(self):
        for trigger in self.__triggerList:
            if str(trigger[1].execute()) == trigger[2]:
                self.execute()
    
    # This code exists to test the proof of concept availability implementation. It is not part of the 'regular' system, but we are using it to purposefully remove the reference to the appliance on a specific command object. This is why it is allowed to make an assumption on the other object, it is a debugging method.
    def debugForceRemoveApplianceInFirstCommandObject(self):
        del self.__taskList[0][0]._appliance