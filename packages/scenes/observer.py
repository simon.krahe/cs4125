from abc import ABC, abstractmethod

class Observer(ABC):
    @abstractmethod
    def update(self):
        pass

class Subject(ABC):
    @abstractmethod
    def attach(self):
        pass
    
    @abstractmethod
    def detach(self):
        pass

    @abstractmethod
    def notify(self):
        pass
