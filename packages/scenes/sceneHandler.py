from scenes.invoker import Invoker
from scenes.sceneAdder import SceneAdder
from notBusinessLayer.handler import HandlerClass
from notBusinessLayer.customIO import CustomIO as IO
from scenes.exceptions import AlreadyExists, NotFound

class SceneHandler(HandlerClass):
    def __init__(self):
        self.__invoker = Invoker()
        self.__sceneAdder = SceneAdder(self.__invoker)
        self.__sceneList = []

    def getInvoker(self):
        return self.__invoker

    def getSceneList(self):
        return self.__sceneList

    def getSceneListString(self):
        result=[]
        for scene in self.__sceneList:
            result.append(scene.getIdentifier())
        return result

    def getScene(self, identifier):
        try:
            return next((scene for scene in self.__sceneList if (scene.getIdentifier()==identifier)), None)
        except NotFound:
            raise NotFound
        except AttributeError:
            raise NotFound

    def triggerScene(self, identifier):
        try:
            scene=self.getScene(identifier)
            if(scene==None):
                raise NotFound
            else:
                scene.execute()
        except NotFound:
            IO.output("Scene " + identifier + " not found")

    def undoScene(self):
        self.__invoker.undoScene()

    def addScene(self, applianceIdentifierList, identifier, taskList, type):
        found=self.getScene(identifier)
        try:
            if found == None:
                return self.__sceneAdder.addScene(identifier, applianceIdentifierList, [taskList], type)
            else:
                raise AlreadyExists
        except AlreadyExists:
            IO.output("Scene " + identifier + " already exists") 
    
    def addNormalScene(self, applianceIdentifierList, identifier, taskList):
        scene = self.addScene(applianceIdentifierList, identifier, taskList, "normal")
        self.__sceneList.append(scene)

    def addDefaultScene(self, applianceIdentifierList, actionIdentifier, taskList):
        applianceIdentifier=str(applianceIdentifierList[0])
        scene = self.addScene(applianceIdentifierList, applianceIdentifier +  ": " + actionIdentifier[0], [taskList], "default")
        self.__sceneList.append(scene)
    
    def addRemovalScene(self, applianceIdentifierList, taskList):
        scene = self.addScene(applianceIdentifierList, applianceIdentifierList[0] + ": " + "Prepare to remove from System", taskList, "removal")
        self.__sceneList.append(scene)

    def removeScene(self, identifier):
        #Potential future TODO: removal scenes cannot be removed before their corresponding appliance is already removed
        found=self.getScene(identifier)
        try:
            if found != None:
                self.__sceneList.remove(found)
            else:
                raise NotFound
        except NotFound:
            IO.output("Scene " + identifier + " not found") 
    
    def removeAssociatedScenes(self, identifier):
        #Potentially implemented in the future by Baptiste
        pass