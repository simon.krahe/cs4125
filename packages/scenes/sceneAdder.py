from scenes.scene import Scene

class SceneAdder:
    def __init__(self, invoker):
        self.__invoker = invoker

    def addScene(self, identifier, applianceIdentifierList, taskList, type):
        from startup import Startup
        return Scene(applianceIdentifierList, identifier, taskList, type, self.__invoker, Startup.getMediator())