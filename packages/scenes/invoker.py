from notBusinessLayer.customIO import CustomIO as IO
from scenes.exceptions import SceneError

class Invoker:
    def __init__(self):
        self.__executedSceneList = []

#The invoker is called from the scene. It needs to be passed the command objects directly in order to avoid circular dependency.
    def trigger(self, listOfCommandObjects):
        #If the list of command objects is a list within a list, then it needs to be unboxed first. This is needed because of the way we have to set the argument type earlier in the ccu (forcing it to be a list [by handing it over like this]. If it already was a list, we now have a list within a list.).
        if isinstance(listOfCommandObjects[0], list):
            listOfCommandObjects=listOfCommandObjects[0]
        for comm in listOfCommandObjects:
            try:
                comm.execute()
            except AttributeError:
                IO.output("Action could not be executed, moving on to next one")

    #Not completely implemented
    def undoScene(self):
        try:
            if len(self.__executedSceneList) == 0:
                raise SceneError
            else:
                IO.output('Undoing scene')
        except SceneError:
            IO.output('No scene to undo')
    #Additional undo functionality could be added here. Will not be added in this project though.
    #Note on implementation possibility (Simon): The invoker is unaware of scenes, but you can group command objects that are passed together. You should probably save history on every appliance so that you can undo changes on an appliance level.