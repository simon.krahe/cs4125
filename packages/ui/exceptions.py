#source: https://docs.python.org/3/tutorial/errors.html
class Error(Exception):
    pass

class NoneLeftError(Error):
    pass

class GetChoiceError(Error):
    pass