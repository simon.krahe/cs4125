from typing import Type
from scenes.sceneHandler import SceneHandler
from applianceSystem.applianceHandler import ApplianceHandler
from users.userHandler import UserHandler
from ui.exceptions import NoneLeftError
from ui.exceptions import GetChoiceError

class UI:
    def __init__(self, applianceHandler, sceneHandler, userHandler):
        self.__sceneHandler=sceneHandler
        self.__applianceHandler=applianceHandler
        self.__userHandler=userHandler
        userInput=""
        userType=""
        userTypeFound=False
        while(userTypeFound == False):
            print("Are you an A)dministrator or an I)nhabitant?")
            userInput=str(input())
            if userInput.lower()=="a":
                userType="Admin"
                userTypeFound=True
            elif userInput.lower()=="i":
                userType="Inhabitant"
                userTypeFound=True
        print("Please login with your username")
        userInput=str(input())
        if userType=="Admin":
            self.__user=self.__userHandler.addAdmin(userInput)
        else:
            self.__user=self.__userHandler.addInhabitant(userInput)

    def arrayUnbox(self, array):
        try:
            if isinstance(array[0][0], list):
                return array[0]
            return array
        except IndexError:
            raise NoneLeftError

    def getChoiceAppend(self, strings):
        strings=self.arrayUnbox(strings)
        userInput=""
        result=[]
        while(userInput.lower()!="n"):
            n=self.getChoice(strings)
            if (0<=n and n<len(strings)):
                result.append(n)
            print("Continue? anything/N")
            userInput=input()
        return result

    def getChoice(self, strings):
        try:
            strings=self.arrayUnbox(strings)
            if (strings==None or len(strings)==0):
                raise NoneLeftError
            userInput=''
            while(not userInput.isnumeric()):
                c=1
                for line in strings:
                    print(str(c) + ") " + line)
                    c+=1
                userInput=input()
                n=int(userInput)-1
            if (0<=n and n<len(strings)):
                return n
            raise GetChoiceError
        except TypeError:
            raise GetChoiceError
        except ValueError:
            raise GetChoiceError
    
    def triggerScene(self):
        sceneList=self.__sceneHandler.getSceneListString()
        print("Please type the name of the scene you want to trigger")
        choice=self.getChoice(sceneList)
        self.__user.triggerScene(sceneList[choice])
    
    def addAppliance(self):
        print("Please specify the type of appliance on the first line and its name on the second")
        userInput=str(input())
        userInput2=str(input())
        try:
            self.__user.addAppliance(userInput, userInput2)
        except ValueError:
            print("That appliance type does not exist")
    
    def removeAppliance(self):
        print("Which appliance do you want to remove?")
        stringList=self.__applianceHandler.getApplianceListString()
        try:
            which=self.getChoice(stringList)
            self.__user.removeAppliance(self.__applianceHandler.getApplianceNameByNumber(which))
        except NoneLeftError:
            print("No appliance left to remove")

    def addScene(self):
        print("Please specify the scene name")
        sceneName=str(input())
        print("Please choose from this list of available commands to be associated with the scene.")
        commandObjectData=self.__applianceHandler.getTaskListWithDescription()
        taskList=[]
        applianceIdentifierList=[]
        commandObjectData=self.arrayUnbox(commandObjectData)
        strings=[]
        for obj in commandObjectData:
            strings.append("Appliance: " + str(obj[0]) + ", Instruction: " + str(obj[1]))
        choice=self.getChoiceAppend(strings)
        for obj in choice:
            taskList.append(commandObjectData[obj][2])
            applianceIdentifierList.append(commandObjectData[obj][1])
        self.__user.addScene(applianceIdentifierList, sceneName, taskList)

    def removeScene(self):
        print("Which one do you want to delete?")
        sceneList=self.__sceneHandler.getSceneListString()
        choice=self.getChoice(sceneList)
        self.__user.removeScene(sceneList[choice])

    def addTriggerToScene(self):
        print("What scene would you like to add a trigger to?")
        sceneList=self.__sceneHandler.getSceneListString()
        sceneChoice=self.getChoice(sceneList)

        print("Please choose from this list of available commands to be the trigger")
        commandObjectData=self.__applianceHandler.getTaskListWithDescription()
        commandObjectData=self.arrayUnbox(commandObjectData)
        strings=[]
        for obj in commandObjectData:
            strings.append("Appliance: " + str(obj[0]) + ", Instruction: " + str(obj[1]))
        commandObjectChoice=self.getChoice(strings)
        applianceIdentifier=commandObjectData[commandObjectChoice][0]
        commandObject=commandObjectData[commandObjectChoice][2]

        print("What value should this property have to trigger the scene?")
        targetValue=input()
        self.__user.addTrigger(sceneList[sceneChoice], commandObject, targetValue, applianceIdentifier)

    def addExternalService(self):
        print("Please specify the type of external service on the first line and its name on the second")
        userInput=str(input())
        userInput2=str(input())
        self.__user.addExternalService(userInput, userInput2)
    
    def addScheduledEvent(self):
        print("What scene would you like to trigger?")
        sceneList=self.__sceneHandler.getSceneListString()
        choice=self.getChoice(sceneList)
        print("How many minutes from now should it trigger?")
        userInput=int(input())
        self.__user.addScheduledEvent(choice, userInput)

    def notYetAvailable(self):
        print("This has not been implemented yet")

    def main(self):
        #Main UI loop
        cont=True
        while(cont):
            try:
                print("Q)uit anytime")
                print("As inhabitant, you can trigger a scene (T)")
                print("As Admin, you can also add an appliance (AA), remove an appliance (RA), add a scene (AS), remove a scene (RS), add a trigger to a scene (AT), add an external service (AE), and add a scheduled event (S)")
                userInput=str(input())
                if(userInput.lower()=="t"):
                    self.triggerScene()
                elif(userInput.lower()=="aa"):
                    self.addAppliance()
                elif(userInput.lower()=="ra"):
                    self.removeAppliance()
                elif(userInput.lower()=="as"):
                    self.addScene()
                elif(userInput.lower()=="rs"):
                    self.removeScene()
                elif(userInput.lower()=="at"):
                    self.addTriggerToScene()
                elif(userInput.lower()=="ae"):
                    self.addExternalService()
                elif(userInput.lower()=="s"):
                    self.addScheduledEvent()
                elif userInput.lower() == "q":
                    cont=False

            except GetChoiceError:
                print("Invalid choice")
            except NoneLeftError:
                print("There are no elements")
            except AttributeError:
                print("This method produced an error. Are you authorized to use it?")
