from appliances.binaryLightbulb import BinaryLightbulb
from appliances.osramLightbulb import OsramLightbulb
from appliances.thermostat import Thermostat
from appliances.toaster import Toaster

#This is an implementation of the factory method to create appliances. It uses conditional type logic as we can't employ polymorphism since we are emphasising the appliance's ignorance of our system and how it behaves. This is why the cannot share a superclass or an interface.
class ApplianceAdder:
    @staticmethod
    def addAppliance(cls, typeOfAppliance, identifier):
        adderMethod = cls.getAdderMethod(cls, typeOfAppliance)
        return adderMethod(cls, identifier)

    @staticmethod
    def getAdderMethod(cls, typeOfAppliance):
        if typeOfAppliance == 'OsramLightbulb':
            return cls.addOsramLightbulb
        if typeOfAppliance == 'Toaster':
            return cls.addToaster
        if typeOfAppliance == 'BinaryLightbulb':
            return cls.addBinaryLightbulb
        if typeOfAppliance == 'Thermostat':
            return cls.addThermostat
        else:
            raise ValueError(typeOfAppliance)

#EVERY ADDER METHOD MUST TAKE IDENTIFIER AS AN ARGUMENT, even if they don't need it.
    @staticmethod
    def addOsramLightbulb(cls, identifier):
        return OsramLightbulb(identifier)

    @staticmethod
    def addThermostat(cls, identifier):
        return Thermostat(identifier)

#A binary lightbulb is not able to store its own name. The identifier is passed as a paremeter regardless, as the pattern requires it
    @staticmethod
    def addBinaryLightbulb(cls, identifier):
        return BinaryLightbulb()

    @staticmethod
    def addToaster(cls, identifier):
        return Toaster(identifier)