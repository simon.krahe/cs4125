import applianceSystem.command.command as c
from appliances.osramLightbulb import OsramLightbulb

#[[default command objects], [other command objects], [command objects to be executed on removal]]
class CommandClassProvider():
    @classmethod
    def getCommandClasses(cls, typeOfAppliance, appliance):
        if typeOfAppliance == 'OsramLightbulb':
            return [[c.OsramLightbulbTurnOn(appliance), c.OsramLightbulbTurnOff(appliance), c.OsramLightbulbGetOnStatus(appliance), c.OsramLightbulbGetBrightness(appliance), c.OsramLightbulbGetColor(appliance), c.OsramLightbulbToggle(appliance)], [], [c.OsramLightbulbTurnOff(appliance)]]
        if typeOfAppliance == 'Thermostat':
            return [[c.ThermostatGetCurrentTemperature(appliance), c.ThermostatGetTargetedTemperature(appliance)], [], []]
        if typeOfAppliance == 'BinaryLightbulb':
            return [[c.BinaryLightbulbGetOnState(appliance), c.BinaryLightbulbTurnOn(appliance), c.BinaryLightbulbTurnOff(appliance), c.BinaryLightbulbToggle(appliance)], [], [c.BinaryLightbulbTurnOff(appliance)]]
        if typeOfAppliance == 'Toaster':
            return [[c.ToasterToast(appliance), c.ToasterCancelToasting(appliance), c.ToasterGetTemperature(appliance), c.ToasterGetToastingTime(appliance)], [] , [c.ToasterCancelToasting(appliance)]]