from abc import ABC, abstractmethod

class Command(ABC):
    def __init__(self, appliance):
        #The appliance is only protected here due to name mangling issues if it were private. We need it like this so inheritance works properly
        self._appliance=appliance

    @abstractmethod
    def getAction(self):
        pass

    @abstractmethod
    def execute(self):
        pass


class OsramLightbulbTurnOn(Command):
    def execute(self):
        return self._appliance.switchOn()

    def getAction(self):
        return "Turn On"


class OsramLightbulbTurnOff(Command):
    def execute(self):
        return self._appliance.switchOff()

    def getAction(self):
        return "Turn Off"

class OsramLightbulbGetOnStatus(Command):
    def execute(self):
        return self._appliance.getTurnedOn()

    def getAction(self):
        return "Get On Status"

class OsramLightbulbGetBrightness(Command):
    def execute(self):
        return self._appliance.getBrightness()

    def getAction(self):
        return "Get Brightness"

class OsramLightbulbGetColor(Command):
    def execute(self):
        return self._appliance.getColor()

    def getAction(self):
        return "Get Color"

class OsramLightbulbToggle(Command):
    def execute(self):
        return self._appliance.toggleOn()

    def getAction(self):
        return "Toggle On State"

class ThermostatGetCurrentTemperature(Command):
    def execute(self):
        return self._appliance.getCurrentTemp()

    def getAction(self):
        return "Get current temperature reading"


class ThermostatGetTargetedTemperature(Command):
    def execute(self):
        return self._appliance.getTargetedTemp()

    def getAction(self):
        return "Get target temperature reading"

#The binary lightbulb requires a license key as a paremeter for each instruction. This is decoupled from the rest of the system and only relevant here -> command design pattern in action

class BinaryLightbulbGetOnState(Command):
    def execute(self):
        return self._appliance.getTurnedOn("cs4125")

    def getAction(self):
        return "Get On State"
        
class BinaryLightbulbTurnOn(Command):
    def execute(self):
        return self._appliance.on("cs4125")

    def getAction(self):
        return "Turn On"

class BinaryLightbulbTurnOff(Command):
    def execute(self):
        return self._appliance.off("cs4125")

    def getAction(self):
        return "Turn Off"

class BinaryLightbulbToggle(Command):
    def execute(self):
        return self._appliance.toggle("cs4125")

    def getAction(self):
        return "Toggle"

class ToasterToast(Command):
    def execute(self):
        return self._appliance.toast()

    def getAction(self):
        return "Start Toasting"

class ToasterCancelToasting(Command):
    def execute(self):
        return self._appliance.cancelToasting()

    def getAction(self):
        return "Cancel Current Toast"

class ToasterGetTemperature(Command):
    def execute(self):
        return self._appliance.getTemperature()

    def getAction(self):
        return "Get Target Temperature"

class ToasterGetToastingTime(Command):
    def execute(self):
        return self._appliance.getTimer()

    def getAction(self):
        return "Get Toasting Time"