from applianceSystem.applianceAdder import ApplianceAdder
from notBusinessLayer.handler import HandlerClass
from notBusinessLayer.customIO import CustomIO as IO
from applianceSystem.exceptions import AlreadyExists, NotFound
from applianceSystem.command.commandClassProvider import CommandClassProvider
from applianceSystem.applianceGrouper import ApplianceGrouper

class ApplianceHandler(HandlerClass):
    def __init__(self, sceneHandler):
        self.__applianceList = [] #contains ApplianceGrouper objects
        self.__sceneHandler = sceneHandler

    def getApplianceListString(self):
        result=[]
        for appl in self.__applianceList:
            result.append(appl.getIdentifier())
        return result

    def getApplianceNameByNumber(self, number):
        return self.__applianceList[number].getIdentifier()

    def addAppliance(self, typeOfAppliance, identifier):
        # Making sure that we do not already have this appliance
        try:
            if (self.getApplianceGroupObject(identifier) != None):
                raise AlreadyExists
            else:
                # Referring to a factory class here
                applianceAdded=ApplianceAdder.addAppliance(ApplianceAdder, typeOfAppliance, identifier)
                #the following outputs an array that contains [[default command objects], [non-default command objects], [command objects to be triggered on appliance removal]]
                commandClasses=CommandClassProvider.getCommandClasses(typeOfAppliance, applianceAdded)
                applianceGroupObject=ApplianceGrouper(applianceAdded, identifier, commandClasses[0], commandClasses[1], typeOfAppliance)
                self.__applianceList.append(applianceGroupObject)
                self.__sceneHandler.addRemovalScene([applianceGroupObject.getIdentifier()], commandClasses[2])

                for i in range(len(applianceGroupObject.getDefaultCommandObjectsList())):
                    self.__sceneHandler.addDefaultScene([applianceGroupObject.getIdentifier()], [applianceGroupObject.getDefaultCommandObjectsList()[i].getAction()], applianceGroupObject.getDefaultCommandObjectsList()[i])
                    #what this does: hands a string (e.g. "Kitchen Lightbulb 1: Turn On") as the scene identifier and the command object to do that to scene creation
                IO.output("Added appliance " + identifier)
        except AlreadyExists:
                IO.output("Appliance " + identifier + " already exists")
        
    def getApplianceGroupObject(self, identifier):
        try:
            return next((appl for appl in self.__applianceList if (appl.getIdentifier()==identifier)), None) 
        except AttributeError:
            raise NotFound

    def removeAppliance(self, identifier):
        try:
            appl = self.getApplianceGroupObject(identifier)
            if appl==None:
                raise NotFound
            self.__sceneHandler.triggerScene(str(identifier) + ": Prepare to remove from System")
            self.__applianceList.remove(appl)
            self.__sceneHandler.removeAssociatedScenes(identifier)
            IO.output("Removed appliance " + identifier)
        except NotFound:
            IO.output("Appliance " + identifier + " could not be removed, not found")

    def getTaskListWithDescription(self):
        # We are aware that this is not good seperation. However, as this calls several other parts of the system, we still felt that it was more appropriate to place it here instead of the UI, so that the UI has to be less 'aware' of how the system works internally.
        result=[] #Array layout: [Appliance Name, Command Object Name, Command Object]
        for appliance in self.__applianceList:
            for command in appliance.getDefaultCommandObjectsList():
                if [appliance.getIdentifier(), command.getAction(), command] not in result:
                    result.append([appliance.getIdentifier(), command.getAction(), command])
            for command in appliance.getOtherCommandObjectsList():
                if [appliance.getIdentifier(), command.getAction(), command] not in result:
                    result.append([appliance.getIdentifier(), command.getAction(), command])
        return result
