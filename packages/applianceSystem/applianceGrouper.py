class ApplianceGrouper():

    def __init__(self, appliance, applianceIdentifier, defaultCommandObjectsList, otherCommandObjectsList, typeOfAppliance):
        self.__appliance=appliance
        self.__applianceIdentifier=applianceIdentifier
        self.__defaultCommandObjectsList=defaultCommandObjectsList
        self.__otherCommandObjectsList=otherCommandObjectsList
        self.__typeOfAppliance=typeOfAppliance
    
    def getAppliance(self):
        return self.__appliance

    def getIdentifier(self):
        return self.__applianceIdentifier
    
    def getDefaultCommandObjectsList(self):
        return self.__defaultCommandObjectsList
    
    def getOtherCommandObjectsList(self):
        return self.__otherCommandObjectsList
    
    def getTypeOfAppliance(self):
        return self.__typeOfAppliance