#source: https://docs.python.org/3/tutorial/errors.html
class Error(Exception):
    pass

class AlreadyExists(Error):
    pass

class NotFound(Error):
    pass