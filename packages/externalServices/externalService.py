from scenes.sceneHandler import SceneHandler
from applianceSystem.applianceGrouper import ApplianceGrouper
from notBusinessLayer.customIO import CustomIO as IO

class ExternalServices:
    def __init__ (self, sceneHandler):
        self._sceneHandler = sceneHandler

    def triggerScene(self, identifier):
        self._sceneHandler.triggerScene(identifier)

    @classmethod
    def login(cls, username, password):
        if username != "":
            if password != "":
                IO.output(username + ' is connected')
                return True
            else :
                IO.output('your password is incorrect')
        else :
            IO.output('your username is incorrect')
        return False
