from externalServices.externalService import ExternalServices
from notBusinessLayer.customIO import CustomIO as IO

class GoogleHome(ExternalServices):
    def __init__ (self, sceneHandler):
        super().__init__(sceneHandler)
        
    def receiveOrder(self, order):
        if order == "Listen to music":
            IO.output("Google Home : music launched")
        else :
            IO.output("Google Home can not execute this order")

    def configurationGoogleHome(self, order):
        if order == "Configuration of the voice":
            IO.output("Google Home : configuration of the voice")
        else :
            IO.output("Google Home can not change this configuration")
