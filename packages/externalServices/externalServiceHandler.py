from notBusinessLayer.customIO import CustomIO as IO
from notBusinessLayer.handler import HandlerClass 
from externalServices.googleHome import GoogleHome
from externalServices.amazonService import Amazon
from externalServices.externalService import ExternalServices


class ExternalServiceHandler(HandlerClass):
    def __init__(self, sceneHandler):
        self.__sceneHandler = sceneHandler
        self.__externalServiceIdentifierList = []

    def addExternalService(self, typeOfExternalService, identifier):                                       
        externalServiceName = identifier
        if(externalServiceName not in self.__externalServiceIdentifierList):
            if typeOfExternalService == "Amazon":
                self.__externalServiceIdentifierList.append(externalServiceName)
                externalServiceObject=Amazon(self.__sceneHandler)
                IO.output(externalServiceName + " is added.")
                return externalServiceObject
            if typeOfExternalService == "GoogleHome":
                self.__externalServiceIdentifierList.append(externalServiceName)
                externalServiceObject=GoogleHome(self.__sceneHandler)
                IO.output(externalServiceName + " is added.")
                return externalServiceObject
        else:
            IO.output("This external service already exists")

# The external service was added because we started to working on it but it is not a complet feature and
# that was why there is only an adder method on the external service file
