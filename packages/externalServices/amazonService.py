from externalServices.externalService import ExternalServices
from notBusinessLayer.customIO import CustomIO as IO

class Amazon(ExternalServices):
    def __init__ (self, sceneHandler):
        super().__init__(sceneHandler)
        
    def receiveOrder (self, order, username, password):
        if super().login(username, password):
            IO.output('Order: ' + order + ' is done')

