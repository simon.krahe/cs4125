#Instead of using print all over our program, we are using this method to output text. This should be an example for programming to interfaces and how we seperate business logic from UI logic. Appliances still use print(), as they are unaware of how our system works.
class CustomIO:
    @classmethod
    def output(cls, arg):
        print(arg)