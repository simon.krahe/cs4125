#  Git Workflow
## Feature Branches
We use feature branches. Each branch should be used for one feature and then discarded ater merge.
## Starting Point
Have a local clone of the repository. Before making changes, get any new changes:
```console
git checkout main
git pull origin main
```
## Creating a Branch
Create your own local branch to make changes in. Naming scheme: "person-feature_description" (e.g. "simon-fridge_communication").
```console
git checkout -b username-feature_description
```
## Making Changes
View changed/new/deleted files with 
```console
git status
```
Stage changed and new files with 
```console
git add (file)
``` 
or use 
```console
git add *
```
to add all displayed files. Use
```console
git rm (file)
  ```
to stage the deletion of files and folers.

Create a commit with the changes using
```console
git commit -m "Commit message"
```
Feel free to split a feature into several commits.
## Merging with Main
Before pushing our changes, we should make sure that we are up to date with the main branch and merge with it.
```console
git checkout main
git pull
git checkout feature-branch
git merge main
```
This might create an additional merge commit. The default name for this is fine, we are squashing commits later anyways.
Normally, git should be handle most merges on itself. For manual merging, here is a [merge conflict guide](https://www.atlassian.com/git/tutorials/using-branches/merge-conflicts)

## Pushing

We push our local changes to gitlab

```console
git push origin branch_name
```
## Create a merge request
Use the gitlab website. Don't forget to tick the box to squash commits after the branch has been merged. If not done automatically, you can delete the feature branch after merge.
# Credits
Inspired by [this readme on a project given as an example](https://github.com/dannoonan/BikeRentalSystem-Analysis-Design/blob/master/README.md)
